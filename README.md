# Card

A "41" card game; a fork of [the Flutter version](https://gitlab.com/card-game-daud/card-flutter) that only have single-player mode. This is made in order to have a version of the game that doesn't depend on Firebase. Not using Firebase means this version can be used on Windows and Linux too. It also isn't limited by Firebase's cap for the free tier and is planned to be released on Play Store at some point.

Planned new features:
* Choosing user profile
* Saving and loading game
* AI play-style can be set by the player