import 'package:flutter/material.dart';

enum ChosenTheme {
  dark, light, system;

  static ChosenTheme? fromOrdinal(int? ord) {
    return (ord != null && 0 <= ord && ord <= 2) ? ChosenTheme.values[ord] : null;
  }

  ThemeMode toThemeMode() {
    switch (this) {
      case ChosenTheme.dark: return ThemeMode.dark;
      case ChosenTheme.light: return ThemeMode.light;
      case ChosenTheme.system: return ThemeMode.system;
    }
  }
}