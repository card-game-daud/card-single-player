import 'package:flutter/material.dart';

ButtonStyle generateWarningButtonStyle(BuildContext context) {
  return ElevatedButton.styleFrom(
      backgroundColor: Theme.of(context).colorScheme.errorContainer,
      foregroundColor: Theme.of(context).colorScheme.onErrorContainer
  );
}