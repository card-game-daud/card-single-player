import 'package:card_flutter/ui/home/components/name_change_dialog.dart';
import 'package:card_flutter/ui/play/play_screen.dart';
import 'package:card_flutter/ui/shared/components/theme_change_fab.dart';
import 'package:flutter/material.dart';

import '../shared/components/loading_indicator.dart';
import 'components/home_main_content.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool hasCheckedOngoingGame = false;

  bool get _isLoading => false;

  void _openChangeNameDialog(String curName) {
    showDialog(
        context: context, builder: (BuildContext context) => NameChangeDialog(curName: curName)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      fit: StackFit.expand,
      children: [
        HomeMainContent(
          onChooseSinglePlayer:
              () => Navigator.push(context, MaterialPageRoute(builder: (_) => const PlayScreen())),
          onChooseChangeName: () => _openChangeNameDialog("Player"),
        ),
        const Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ThemeChangeFloatingActionButton()
            ),
          ],
        ),
        if (_isLoading) Container(color: Theme.of(context).colorScheme.background.withOpacity(0.5),),
        if (_isLoading) const LoadingIndicator(),
      ],
    );
  }
}