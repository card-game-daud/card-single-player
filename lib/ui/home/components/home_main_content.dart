import 'package:flutter/material.dart';

class HomeMainContent extends StatefulWidget {
  final VoidCallback onChooseSinglePlayer;
  final VoidCallback onChooseChangeName;

  const HomeMainContent({
    super.key,
    required this.onChooseSinglePlayer,
    required this.onChooseChangeName
  });

  @override
  State<StatefulWidget> createState() => _SignedInHomeState();
}

class _SignedInHomeState extends State<HomeMainContent> {
  bool get _isLoading => false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.background,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 16.0),
            child: Text(
              "Game Kartu 41",
              style: Theme.of(context).textTheme.titleLarge!.copyWith(fontSize: 40.0),
            ),
          ),
          ElevatedButton(
            onPressed: (_isLoading)
                ? null : widget.onChooseSinglePlayer,
            child: const Text("New Game")
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: _NameChangeButton(widget.onChooseChangeName)
          ),
        ],
      ),
    );
  }
}

class _NameChangeButton extends StatefulWidget {
  final VoidCallback onClick;

  const _NameChangeButton(this.onClick);

  @override
  State<StatefulWidget> createState() => _NameChangeButtonState();
}

class _NameChangeButtonState extends State<_NameChangeButton> {
  bool get _isLoading => false;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
          padding: const EdgeInsets.all(24.0)
      ),
      onPressed: (_isLoading) ? null : widget.onClick,
      child: Text.rich(
        TextSpan(
          style: Theme.of(context).textTheme.bodyLarge!
            .copyWith(fontSize: 20.0),
          children: [
            const TextSpan(text: "Name: "),
            TextSpan(
              text: "Player",
              style: TextStyle(color: Theme.of(context).colorScheme.primary)
            ),
          ]
        )
      )
    );
  }
}