import 'package:card_flutter/domain/model/lobby_game_model.dart';

class PlayArguments {
  LobbyGameModel? gameData;

  PlayArguments({this.gameData});
}