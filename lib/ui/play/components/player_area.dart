import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../domain/model/card_model.dart';
import '../../../domain/model/player_model.dart';
import 'card_components.dart';

class PlayerArea extends StatefulWidget {
  // Player data
  final int id;
  final bool isUser;
  // Layout configuration
  final bool isVertical;
  final bool flipLayout;
  
  const PlayerArea({
    super.key,
    required this.id,
    required this.isUser,
    required this.isVertical,
    required this.flipLayout
  });

  @override
  State<StatefulWidget> createState() => _PlayerAreaState();
}

class _PlayerAreaState extends State<PlayerArea> {
  @override
  Widget build(BuildContext context) {
    final playVM = Provider.of<PlayViewModel>(context);
    final playerData = playVM.playerData![widget.id];

    final topDiscardedCard = playerData.discardPile.lastOrNull; // For null check
    final isCardVertical = !widget.isVertical;
    final orientation = MediaQuery.of(context).orientation;

    final cardWidgets = [
      PlayingCardDeck(
        listCard: playerData.deck,
        isHidden: !widget.isUser,
        isVertical: widget.isVertical,
        onTapCard: (card) => _discardCard(playerData, card),
      ),
      if (topDiscardedCard != null) _PlayerAreaDivider(
        isVertical: orientation != Orientation.portrait,
        isLong: (orientation == Orientation.portrait) == widget.isVertical
      ),
      if (topDiscardedCard != null) PlayingCard(
        card: topDiscardedCard,
        isHidden: false,
        isVertical: isCardVertical,
        onTap: () => _drawFromDiscardPile(playerData, topDiscardedCard),
      ),
    ];
    switch (orientation) {
      case Orientation.portrait:
        final List<Widget> children = [
          _PlayerNameText(playerData: playerData),
          ...cardWidgets
        ];
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Wrap(
            direction: Axis.vertical,
            spacing: 8.0,
            crossAxisAlignment: (!widget.flipLayout)
                ? WrapCrossAlignment.end : WrapCrossAlignment.start,
            children: (widget.flipLayout)
                ? children : children.reversed.toList(),
          ),
        );

      case Orientation.landscape:
        final children = [
          _PlayerNameText(playerData: playerData),
          Wrap(
            direction: Axis.horizontal,
            spacing: 8.0,
            crossAxisAlignment: (!widget.flipLayout)
                ? WrapCrossAlignment.start : WrapCrossAlignment.end,
            children: (widget.flipLayout != widget.isVertical)
                ? cardWidgets.reversed.toList() : cardWidgets,
          ),
        ];
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: (!widget.flipLayout)
                ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: (widget.flipLayout != widget.isVertical)
                ? children : children.reversed.toList()
          ),
        );
    }
  }

  void _drawFromDiscardPile(PlayerModel curPlayerData, CardModel drawnCard) {
    if (widget.isUser && curPlayerData.isDrawing()) {
      Provider.of<PlayViewModel>(context, listen: false)
          .drawFromDiscardPile(PlayViewModel.userId, drawnCard);
    }
  }

  void _discardCard(PlayerModel curPlayerData, CardModel drawnCard) {
    if (widget.isUser && curPlayerData.isDiscarding()) {
      Provider.of<PlayViewModel>(context, listen: false)
          .discardCard(PlayViewModel.userId, drawnCard);
    }
  }
}

class _PlayerNameText extends StatelessWidget {
  final PlayerModel playerData;

  const _PlayerNameText({required this.playerData});

  @override
  Widget build(BuildContext context) {
    final playVM = Provider.of<PlayViewModel>(context);
    final isCurrentPlayer = playVM.isCurrentPlayer(playerData.id);

    return Text(
      playerData.name,
      style: Theme.of(context).textTheme.labelMedium
          ?.copyWith(decoration: (isCurrentPlayer) ? TextDecoration.underline : null),
    );
  }
}

class _PlayerAreaDivider extends StatelessWidget {
  final bool isVertical;
  final bool isLong;

  const _PlayerAreaDivider({required this.isVertical, required this.isLong});

  @override
  Widget build(BuildContext context) {
    final length = (isLong) ? PlayingCard.longSideSize : PlayingCard.shortSideSize;
    const double thickness = 1;

    return SizedBox(
      width: (isVertical) ? thickness : length,
      height: (isVertical) ? length : thickness,
      child: Container(
        color: Theme.of(context).colorScheme.onSurface,
      ),
    );
  }
}