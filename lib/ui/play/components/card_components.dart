import 'package:flutter/material.dart';

import '../../../domain/model/card_model.dart';

class PlayingCardDeck extends StatelessWidget {
  final List<CardModel> listCard;
  final bool isHidden;
  final bool isVertical;
  final void Function(CardModel)? onTapCard;

  const PlayingCardDeck({
    super.key,
    required this.listCard,
    required this.isHidden,
    required this.isVertical,
    this.onTapCard
  });

  @override
  Widget build(BuildContext context) {
    final isCardVertical = !isVertical;
    final listCardWidget = listCard.map((card) => PlayingCard(
      card: card,
      isHidden: isHidden,
      isVertical: isCardVertical,
      onTap: () => (onTapCard != null) ? onTapCard!(card) : null,
    )).toList();
    return Wrap(
      direction: (isVertical) ? Axis.vertical : Axis.horizontal,
      spacing: 4.0,
      children: listCardWidget,
    );
  }
}

class PlayingCard extends StatelessWidget {
  final CardModel card;
  final bool isHidden;
  final bool isVertical;
  final VoidCallback? onTap;

  static const shortSideSize = 60.0;
  static const longSideSize = 90.0;

  const PlayingCard({
    super.key,
    required this.card,
    required this.isHidden,
    this.isVertical = true,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return RotatedBox(
      quarterTurns: (isVertical) ? 0 : 1,
      child: GestureDetector(
        onTap: onTap,
        child: Image.asset(
          (isHidden) ? "assets/images/hidden_card.png" : card.image,
          width: shortSideSize,
          height: longSideSize,
        ),
      ),
    );
  }
}