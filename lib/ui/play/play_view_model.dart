import 'dart:async';

import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/model/card_model.dart';
import 'package:card_flutter/domain/model/game_status_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/domain/usecases/play/decide_winner_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_computer_move_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_game_data_use_case.dart';
import 'package:flutter/material.dart';

class PlayViewModel extends ChangeNotifier {
  // Dependencies
  final GenerateGameDataUseCase _generateGameData;
  final GenerateComputerMoveUseCase _generateComputerMove;
  final DecideWinnerUseCase _decideWinner;

  // States
  GameStatus? _gameStatus = GameStatus.starting;
  List<CardModel>? _drawPile;
  List<PlayerModel>? _playerData;
  int? _currentPlayerId;

  // Timer to avoid multiple computer moves being executed at once
  Timer? _computerMoveTimer;

  // Constants
  static const numPlayers = 4;
  static const userId = 0;
  static const computerMoveDuration = Duration(milliseconds: 500);

  PlayViewModel(
      this._generateGameData,
      this._generateComputerMove,
      this._decideWinner,
  );

  // Getter for states that need to be accessed by the views
  GameStatus? get gameStatus => _gameStatus;
  List<CardModel>? get drawPile => _drawPile;
  List<PlayerModel>? get playerData => _playerData;

  bool isCurrentPlayer(int id) {
    return id == _currentPlayerId;
  }

  void _updateGameData(
      GameStatus gameStatus,
      List<CardModel> drawPile,
      List<PlayerModel> playerData,
      int currentPlayerId
  ) {
    _gameStatus = gameStatus;
    _drawPile = drawPile;
    _playerData = playerData;
    _currentPlayerId = currentPlayerId;
    notifyListeners();
  }

  void setupGame({LobbyGameModel? startingData, String? userKey}) {
    startGame();
  }

  Future<void> startGame({LobbyGameModel? lobbyGameData, int userRelativeId = 0}) async {
    final (playerData, drawPile, currentPlayerId) = _generateGameData.invoke(
        numPlayers, lobbyGameData, AnonymousUserModel("id-dummy", "Player")
    );

    _updateGameData(GameStatus.ongoing, drawPile, playerData, currentPlayerId);

    if (currentPlayerId != userId && !playerData[currentPlayerId].isHuman()) {
      Timer(computerMoveDuration, () => _computerMove(currentPlayerId));
    }
  }

  Future<void> drawFromDrawPile(int curPlayerId, CardModel drawnCard) async {
    final updatedPlayerData = playerData!;
    updatedPlayerData[curPlayerId].deck.add(drawnCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.discarding;
    final updatedDrawPile = drawPile!;
    updatedDrawPile.remove(drawnCard);

    _updateGameData(gameStatus!, updatedDrawPile, updatedPlayerData, _currentPlayerId!);
  }

  Future<void> drawFromDiscardPile(int curPlayerId, CardModel drawnCard) async {
    final updatedPlayerData = playerData!;
    updatedPlayerData[curPlayerId].deck.add(drawnCard);
    updatedPlayerData[curPlayerId].discardPile.remove(drawnCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.discarding;

    _updateGameData(gameStatus!, drawPile!, updatedPlayerData, curPlayerId);
  }

  Future<void> discardCard(int curPlayerId, CardModel discardedCard) async {
    final updatedPlayerData = playerData!;
    final nextPlayerId = (curPlayerId + 1) % numPlayers;
    updatedPlayerData[curPlayerId].deck.remove(discardedCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.waiting;
    updatedPlayerData[nextPlayerId].discardPile.add(discardedCard);
    updatedPlayerData[nextPlayerId].turnStatus = TurnStatus.drawing;

    _updateGameData(gameStatus!, drawPile!, updatedPlayerData, nextPlayerId);

    if (
      updatedPlayerData[curPlayerId].calculateScore() == PlayerModel.maxScore || drawPile!.isEmpty
    ) {
      _endGame();
    } else {
      if (!updatedPlayerData[nextPlayerId].isHuman()) {
        Timer(computerMoveDuration, () => _computerMove(nextPlayerId));
      }
    }
  }

  void _computerMove(int playerId) {
    final computerMove = _generateComputerMove.invoke(playerData![playerId]);

    if (_computerMoveTimer != null) {
      _computerMoveTimer?.cancel();
    }

    _computerMoveTimer = Timer(computerMoveDuration, () async {
      switch (computerMove.$1) {
        case DrawSource.drawPile:
          await drawFromDrawPile(playerId, drawPile!.last);
        case DrawSource.discardPile:
          await drawFromDiscardPile(playerId, playerData![playerId].discardPile.last);
      }

      Timer(computerMoveDuration, () => discardCard(playerId, computerMove.$2));
    });
  }

  Future<void> _endGame() async {
    final updatedPlayerData = playerData!;
    for (var i = 0; i < updatedPlayerData.length; i++) {
      updatedPlayerData[i].setScore();
    }

    _updateGameData(GameStatus.finishing, drawPile!, updatedPlayerData, _currentPlayerId!);
  }

  void restartGame() {
    _updateGameData(GameStatus.starting, drawPile!, playerData!, _currentPlayerId!);
  }

  void doneShowingGameOverDialog() {
    _gameStatus = GameStatus.gameover;
  }

  String get winnerName {
    final winnerNames = _decideWinner.invoke(playerData!)
        .map((player) => player.name);
    return switch (winnerNames.length) {
      1 => winnerNames.single,
      2 => winnerNames.join(" and "),
      3 || 4 => "${winnerNames.take(winnerNames.length-1).join(", ")} and ${winnerNames.last}",
      _ => ""
    };
  }

  Future<void> leaveScreen(bool chooseSave) async {
    // Manually reset states
    // Use delay to make sure the screen has been left first
    const int delay = 1;
    Timer(const Duration(seconds: delay), () {
      _gameStatus = GameStatus.starting;
      _drawPile = null;
      _playerData = null;
      _currentPlayerId = null;
      _computerMoveTimer = null;
      notifyListeners();
    });
  }
}