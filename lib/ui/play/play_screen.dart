import 'package:card_flutter/domain/model/card_model.dart';
import 'package:card_flutter/domain/model/game_status_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/ui/play/components/card_components.dart';
import 'package:card_flutter/ui/play/components/game_over_dialog.dart';
import 'package:card_flutter/ui/play/components/player_area.dart';
import 'package:card_flutter/ui/play/play_route.dart';
import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:card_flutter/ui/shared/components/theme_change_fab.dart';
import 'package:card_flutter/ui/shared/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'components/confirm_leave_dialog.dart';

class PlayScreen extends StatefulWidget {
  const PlayScreen({super.key});

  @override
  State<StatefulWidget> createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  bool showDisconnectedDialog = false;

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)?.settings.arguments as PlayArguments?;
    final startingData = arguments?.gameData;

    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) {
        if (!didPop) {
          _showConfirmLeaveDialog();
        }
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: _getPlayScreenBackground()
        ),
        child: Consumer<PlayViewModel>(
          builder: (context, playViewModel, w) {
            // For smart cast purpose
            final playerData = playViewModel.playerData;
            final drawPileTop = playViewModel.drawPile?.lastOrNull;

            SchedulerBinding.instance.addPostFrameCallback((timeStamp)
              => _launchGameStatusSideEffect(playViewModel.gameStatus, startingData)
            );

            return Stack(
              children: (playerData != null) ? [
                Container(
                  alignment: Alignment.bottomCenter,
                  child: const PlayerArea(
                    id: 0,
                    isUser: true,
                    isVertical: false,
                    flipLayout: false,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: const PlayerArea(
                    id: 1,
                    isUser: false,
                    isVertical: true,
                    flipLayout: true,
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: const PlayerArea(
                    id: 2,
                    isUser: false,
                    isVertical: false,
                    flipLayout: true,
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: const PlayerArea(
                    id: 3,
                    isUser: false,
                    isVertical: true,
                    flipLayout: false,
                  ),
                ),
                if (drawPileTop != null) Container(
                  alignment: Alignment.center,
                  child: PlayingCard(
                    card: drawPileTop,
                    isHidden: true,
                    onTap: () => _drawFromDrawPile(playerData[0], drawPileTop),
                  ),
                ),
                ..._generateFABList()
              ] : _generateFABList(),
            );
          },
        )
      )
    );
  }

  List<Widget> _generateFABList() {
    return [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          backgroundColor: Theme.of(context).colorScheme.primary.withAlpha(96),
          foregroundColor: Colors.white60,
          heroTag: null,
          onPressed: () => Navigator.maybePop(context),
          child: const Icon(Icons.arrow_back),
        ),
      ),
      Container(
        alignment: Alignment.bottomRight,
        padding: const EdgeInsets.all(8.0),
        child: const ThemeChangeFloatingActionButton()
      )
    ];
  }
  
  RadialGradient _getPlayScreenBackground() {
    return (Theme.of(context).brightness == Brightness.dark)
        ? playScreenBackgroundDark : playScreenBackgroundLight;
  }

  void _showConfirmLeaveDialog() {
    showDialog(context: context, builder: (context) =>
        ConfirmLeaveDialog(
          askToSave: false,
          onConfirm: (chooseSave) => _exitScreen(chooseSave),
        )
    );
  }

  void _launchGameStatusSideEffect(GameStatus? gameStatus, LobbyGameModel? startingData) {
    final playViewModel = Provider.of<PlayViewModel>(context, listen: false);

    if (gameStatus == GameStatus.starting) {
      const userKey = "id-dummy";
      playViewModel.setupGame(
          startingData: startingData, userKey: userKey
      );

    } else if (gameStatus == GameStatus.finishing) {
      playViewModel.doneShowingGameOverDialog();
      showDialog(context: context, builder: (context) =>
          GameOverDialog(
            onExit: (chooseSave) => _exitScreen(chooseSave),
          )
      );
    }
  }

  void _drawFromDrawPile(PlayerModel curPlayerData, CardModel drawPileTop) {
    if (curPlayerData.isDrawing()) {
      Provider.of<PlayViewModel>(context, listen: false)
          .drawFromDrawPile(PlayViewModel.userId, drawPileTop);
    }
  }

  void _exitScreen(bool chooseSave) {
    Provider.of<PlayViewModel>(context, listen: false)
        .leaveScreen(chooseSave);
    // Needs to wait for state-setting is finished or else the app will crash
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      if (context.mounted) Navigator.pop(context);
    });
  }
}