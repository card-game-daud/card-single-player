import 'card_model.dart';

class PlayerModel {
  int id; // id to decide turn order
  String name;
  String? uid; // user id of a human player
  List<CardModel> deck;
  List<CardModel> discardPile;
  TurnStatus turnStatus;
  int score;

  PlayerModel(
      this.id,
      this.name,
      this.uid,
      this.deck,
      this.discardPile,
      this.turnStatus,
      this.score
  );

  // Default id and name prefix for computer players
  static const computerPlayerPrefix = "COMP ";
  // For cases when the player is human, but the id is unavailable for some reason
  static const defaultHumanId = "DEFAULT_HUMAN_ID";

  // Convenience constants to avoid magic numbers
  static const minScore = -30;
  static const maxScore = 41;

  bool isHuman() {
    return uid != null;
  }

  bool isDrawing() {
    return (
      turnStatus == TurnStatus.drawing // Core requirement
        && deck.length == 4 // Game rule-set validation
    );
  }

  bool isDiscarding() {
    return (
        turnStatus == TurnStatus.discarding // Core requirement
            && deck.length == 5 // Game rule-set validation
    );
  }

  int calculateScore() {
    var maxScore = minScore - 1; // Initial value is the lowest possible score minus 1
    // Calculate the scores for each suit as the primary suit
    for (final suit in Suit.values) {
      var suitScore = 0;
      for (final card in deck) {
        if (card.suit == suit) {
          suitScore += card.rank.value;
        } else {
          suitScore -= card.rank.value;
        }
      }
      if (suitScore > maxScore) {
        maxScore = suitScore;
      }
    }
    return maxScore;
  }

  void setScore() {
    score = calculateScore();
  }
}

enum TurnStatus {
  drawing, discarding, waiting;
}