enum CardModel {
  twoOfDiamonds(Rank.two, Suit.diamond, "assets/images/_2_of_diamonds.png"),
  twoOfClubs(Rank.two, Suit.club, "assets/images/_2_of_clubs.png"),
  twoOfHearts(Rank.two, Suit.heart, "assets/images/_2_of_hearts.png"),
  twoOfSpades(Rank.two, Suit.spade, "assets/images/_2_of_spades.png"),
  threeOfDiamonds(Rank.three, Suit.diamond, "assets/images/_3_of_diamonds.png"),
  threeOfClubs(Rank.three, Suit.club, "assets/images/_3_of_clubs.png"),
  threeOfHearts(Rank.three, Suit.heart, "assets/images/_3_of_hearts.png"),
  threeOfSpades(Rank.three, Suit.spade, "assets/images/_3_of_spades.png"),
  fourOfDiamonds(Rank.four, Suit.diamond, "assets/images/_4_of_diamonds.png"),
  fourOfClubs(Rank.four, Suit.club, "assets/images/_4_of_clubs.png"),
  fourOfHearts(Rank.four, Suit.heart, "assets/images/_4_of_hearts.png"),
  fourOfSpades(Rank.four, Suit.spade, "assets/images/_4_of_spades.png"),
  fiveOfDiamonds(Rank.five, Suit.diamond, "assets/images/_5_of_diamonds.png"),
  fiveOfClubs(Rank.five, Suit.club, "assets/images/_5_of_clubs.png"),
  fiveOfHearts(Rank.five, Suit.heart, "assets/images/_5_of_hearts.png"),
  fiveOfSpades(Rank.five, Suit.spade, "assets/images/_5_of_spades.png"),
  sixOfDiamonds(Rank.six, Suit.diamond, "assets/images/_6_of_diamonds.png"),
  sixOfClubs(Rank.six, Suit.club, "assets/images/_6_of_clubs.png"),
  sixOfHearts(Rank.six, Suit.heart, "assets/images/_6_of_hearts.png"),
  sixOfSpades(Rank.six, Suit.spade, "assets/images/_6_of_spades.png"),
  sevenOfDiamonds(Rank.seven, Suit.diamond, "assets/images/_7_of_diamonds.png"),
  sevenOfClubs(Rank.seven, Suit.club, "assets/images/_7_of_clubs.png"),
  sevenOfHearts(Rank.seven, Suit.heart, "assets/images/_7_of_hearts.png"),
  sevenOfSpades(Rank.seven, Suit.spade, "assets/images/_7_of_spades.png"),
  eightOfDiamonds(Rank.eight, Suit.diamond, "assets/images/_8_of_diamonds.png"),
  eightOfClubs(Rank.eight, Suit.club, "assets/images/_8_of_clubs.png"),
  eightOfHearts(Rank.eight, Suit.heart, "assets/images/_8_of_hearts.png"),
  eightOfSpades(Rank.eight, Suit.spade, "assets/images/_8_of_spades.png"),
  nineOfDiamonds(Rank.nine, Suit.diamond, "assets/images/_9_of_diamonds.png"),
  nineOfClubs(Rank.nine, Suit.club, "assets/images/_9_of_clubs.png"),
  nineOfHearts(Rank.nine, Suit.heart, "assets/images/_9_of_hearts.png"),
  nineOfSpades(Rank.nine, Suit.spade, "assets/images/_9_of_spades.png"),
  tenOfDiamonds(Rank.ten, Suit.diamond, "assets/images/_10_of_diamonds.png"),
  tenOfClubs(Rank.ten, Suit.club, "assets/images/_10_of_clubs.png"),
  tenOfHearts(Rank.ten, Suit.heart, "assets/images/_10_of_hearts.png"),
  tenOfSpades(Rank.ten, Suit.spade, "assets/images/_10_of_spades.png"),
  jackOfDiamonds(Rank.jack, Suit.diamond, "assets/images/jack_of_diamonds.png"),
  jackOfClubs(Rank.jack, Suit.club, "assets/images/jack_of_clubs.png"),
  jackOfHearts(Rank.jack, Suit.heart, "assets/images/jack_of_hearts.png"),
  jackOfSpades(Rank.jack, Suit.spade, "assets/images/jack_of_spades.png"),
  queenOfDiamonds(Rank.queen, Suit.diamond, "assets/images/queen_of_diamonds.png"),
  queenOfClubs(Rank.queen, Suit.club, "assets/images/queen_of_clubs.png"),
  queenOfHearts(Rank.queen, Suit.heart, "assets/images/queen_of_hearts.png"),
  queenOfSpades(Rank.queen, Suit.spade, "assets/images/queen_of_spades.png"),
  kingOfDiamonds(Rank.king, Suit.diamond, "assets/images/king_of_diamonds.png"),
  kingOfClubs(Rank.king, Suit.club, "assets/images/king_of_clubs.png"),
  kingOfHearts(Rank.king, Suit.heart, "assets/images/king_of_hearts.png"),
  kingOfSpades(Rank.king, Suit.spade, "assets/images/king_of_spades.png"),
  aceOfDiamonds(Rank.ace, Suit.diamond, "assets/images/ace_of_diamonds.png"),
  aceOfClubs(Rank.ace, Suit.club, "assets/images/ace_of_clubs.png"),
  aceOfHearts(Rank.ace, Suit.heart, "assets/images/ace_of_hearts.png"),
  aceOfSpades(Rank.ace, Suit.spade, "assets/images/ace_of_spades.png");

  final Rank rank;
  final Suit suit;
  final String image;

  const CardModel(this.rank, this.suit, this.image);
  
}

enum Suit {
  diamond, club, heart, spade
}

enum Rank {
  two(2),
  three(3),
  four(4),
  five(5),
  six(6),
  seven(7),
  eight(8),
  nine(9),
  ten(10),
  jack(10),
  queen(10),
  king(10),
  ace(11);
  
  final int value;

  const Rank(this.value);
}