import '../../model/player_model.dart';

class DecideWinnerUseCase {
  List<PlayerModel> invoke(List<PlayerModel> playerList) {
    var maxScore = -31; // Initial value is the lowest possible score minus 1

    List<PlayerModel> winner = List.empty(growable: true);
    for (final player in playerList) {
      if (player.score > maxScore) {
        maxScore = player.score;
      }
    }
    winner.addAll(playerList.where((player) => player.score == maxScore));

    return winner;
  }
}