import 'dart:math';

import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/model/card_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';

// Generate a 4-element list of PlayerModel, the draw pile, and the starting player
class GenerateGameDataUseCase {
  static const int numCardsPerDeck = 4;

  (List<PlayerModel>, List<CardModel>, int) invoke(
      int numPlayers,
      LobbyGameModel? lobbyGameData,
      AnonymousUserModel? userData
  ) {
    // Generate the list of human players
    final humanList = lobbyGameData?.players.map((e) => (e.id, e.name)).toList()
      ?? [(userData?.id ?? PlayerModel.defaultHumanId, userData?.name ?? "HUMAN")];

    // Generate the list of all players
    final playerUidAndNames = List.generate(numPlayers, (index) {
      return (index >= 0 && index < humanList.length)
          ? (humanList[index].$1, humanList[index].$2)
          : (null, "COMP $index");
      },
      growable: false
    );
    final playerUids = playerUidAndNames.map((e) => e.$1).toList();
    final playerNames = playerUidAndNames.map((e) => e.$2).toList();

    // Shuffle card to generate decks and draw pile
    //  "toList()" method is used to make a copy,
    //  as "CardModel.values" is constant and unshuffleable
    final shuffledCards = CardModel.values.toList();
    shuffledCards.shuffle();

    // Create player decks
    final playerDecks = List.generate(numPlayers, (index) =>
      shuffledCards.getRange(
          index * numCardsPerDeck,
          index * numCardsPerDeck + numCardsPerDeck
      ).toList(),
      growable: false
    );

    // Create draw pile
    final drawPile = List.of(
        shuffledCards.getRange(numPlayers*numCardsPerDeck, shuffledCards.length)
    );
    
    // Choose the first player
    final startingPlayer = Random().nextInt(numPlayers);

    return (
      List.generate(numPlayers, (index) =>
          PlayerModel(
              index,
              playerNames[index],
              playerUids[index],
              playerDecks[index],
              List<CardModel>.empty(growable: true), // Start with empty discard pile
              (startingPlayer == index)
                  ? TurnStatus.drawing : TurnStatus.waiting,
              0 // Start with zero score
          ),
          growable: false
      ),
      drawPile,
      startingPlayer
    );
  }
}