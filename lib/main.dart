import 'package:card_flutter/data/shared_preferences.dart';
import 'package:card_flutter/domain/usecases/play/decide_winner_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_computer_move_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_game_data_use_case.dart';
import 'package:card_flutter/ui/home/home_screen.dart';
import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:card_flutter/ui/shared/theme/theme.dart';
import 'package:card_flutter/ui/shared/view_models/theme_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:window_size/window_size.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setWindowTitle("Kartu 41");
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemeViewModel(AppSharedPreferences())),
        ChangeNotifierProvider(create: (context) => PlayViewModel(
          GenerateGameDataUseCase(),
          GenerateComputerMoveUseCase(),
          DecideWinnerUseCase(),
        )),
      ],
      child: const MyApp()
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Kartu 41",
        theme: ThemeData(
          colorScheme: lightColorScheme,
          useMaterial3: true,
        ),
        darkTheme: ThemeData(
          colorScheme: darkColorScheme,
          useMaterial3: true,
        ),
        themeMode: Provider.of<ThemeViewModel>(context).currentTheme.toThemeMode(),
        home: const MyHomePage(),
      );
  }
}
